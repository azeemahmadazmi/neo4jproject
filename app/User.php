<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use NeoEloquent;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
//use Illuminate\Notifications\Notifiable;

class User extends NeoEloquent implements
    AuthenticatableContract,
    AuthorizableContract, 
    CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword;

    //protected $label = 'User';
     protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','image'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    public function actors()
    {
        return $this->hasMany('App\Models\Actor', 'CREATED');
    }
 
   public function directors()
    {
        return $this->hasMany('App\Models\Director', 'CREATED');
    }
    public function writers()
    {
        return $this->hasMany('App\Models\Writer', 'CREATED');
    }
     public function producers()
    {
        return $this->hasMany('App\Models\Producer', 'CREATED');
    }
     public function movies()
    {
        return $this->hasMany('App\Models\Movie', 'CREATED');
    }
    public function genres()
    {
        return $this->hasMany('App\Models\Genre', 'CREATED');
    }
}
